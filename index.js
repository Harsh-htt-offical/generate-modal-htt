const fs = require('fs');
const path = require('path');

const test = "/Applications/Harsh-Work/workspace/gererate-modal/test-folder/store/index.js";

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function generateModule(moduleName) {
  const modulePath = path.join('pages', moduleName);
  const storePath = path.join('store');
  // const servicesPath = path.join('services');

  // Create the module folder
  fs.mkdirSync(modulePath, { recursive: true });
  fs.mkdirSync(storePath, { recursive: true });
  // fs.mkdirSync(servicesPath, { recursive: true });

  // Generate files from templates
  generateFile('module-template.js', path.join(modulePath, `index.jsx`), capitalizeFirstLetter(moduleName.replace(' ','')));
  generateFile('store-template.js', path.join(storePath, `${moduleName.replace(' ','')}.js`), moduleName.replace(' ',''));
  // generateFile('service-template.js', path.join(servicesPath, `${moduleName.replace(' ','')}.js`), moduleName.replace(' ',''));

  // if (!fs.existsSync(test)) {
  //   generateFile('store-index.js', path.join(storePath, `index.js`), moduleName.replace(' ',''));
  // }else{
  //   updateStoreIndexFile(moduleName);
  // }
}

function generateFile(templateFileName, outputFilePath, moduleName) {
  const templatePath = path.join(__dirname, 'templates', templateFileName);
  
  if (!fs.existsSync(templatePath)) {
    console.error(`Template file '${templateFileName}' not found.`);
    return;
  }

  try {
    const templateContent = fs.readFileSync(templatePath, 'utf-8');
    const fileContent = templateContent.replace(/<moduleName>/g, moduleName);
    fs.writeFileSync(outputFilePath, fileContent, 'utf-8');
    console.log(`Generated file: ${outputFilePath}`);
  } catch (err) {
    console.error(`Error generating file '${outputFilePath}': ${err.message}`);
  }
}

function updateStoreIndexFile(moduleName) {
  const storeIndexPath = path.join(test);
  const storeIndexContent = fs.readFileSync(storeIndexPath, 'utf-8');

  const importStatement = `import ${moduleName.replace(' ','')}Reducer from './${moduleName}';`;
  const reducerKey = `${moduleName.replace(' ','').toLowerCase()}`;

  // Check if the import statement already exists in the store index file
  if (storeIndexContent.includes(importStatement)) {
    console.log(`Import statement for '${moduleName}' already exists in the store index file.`);
    return;
  }

  // Add the import statement
  const updatedStoreIndexContent = storeIndexContent.replace(
    /\/\/ Import Reducers([\s\S]*?)const rootReducers/m,
    (match) => `${importStatement}\n${match}`
  );

  // Check if the reducer key already exists in the root reducer
  if (storeIndexContent.includes(`\n    ${reducerKey}:`)) {
    console.log(`Reducer key '${reducerKey}' already exists in the store index file.`);
    return;
  }

  // Add the reducer to the root reducer
  const updatedRootReducers = updatedStoreIndexContent.replace(
    /const rootReducers = combineReducers\({([\s\S]*?})\);/m,
    (match) => `${match}\n    ${reducerKey}: ${moduleName.replace(' ','')}Reducer,`
  );

  fs.writeFileSync(storeIndexPath, updatedRootReducers, 'utf-8');
  console.log(`Updated store index file: ${storeIndexPath}`);
}

module.exports = generateModule;
