
# generate-modal-htt

This package is used for generating Modal for module wise store and file creation.

Install:
```
npm i generate-modal-htt
```

Add this command on script
```
"generate-modal": "node generate-modal.js"

```

Please create generate-modal.js
```
const moduleName = process.argv[2];

if (!moduleName) {
  console.error('Please provide a module name.');
  process.exit(1);
}
else{
    const myModal = require('generate-modal-htt')
    myModal(moduleName);
}

console.log(`Generating modal for module: ${moduleName}`);
```

How to create modal via commandline:

```
npm run generate-modal <moduleName>

```