import { combineReducers, createStore, applyMiddleware } from 'redux';
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './root-saga';
import { persistStore } from 'redux-persist';

// Import Reducers
import <moduleName>Reducer from './<moduleName>';

const rootReducers = combineReducers({
    <moduleName>: <moduleName>Reducer,
});

// const persistConfig = {
//     key: 'root',
//     storage: AsyncStorage,
// }

// const persistedReducer = persistReducer(persistConfig, rootReducers)
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducers, applyMiddleware(sagaMiddleware));

export const makeStore = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
    store.__persistor = persistStore(store);
    return store;
};

// export default makeStore;

export const wrapper = createWrapper(makeStore, { debug: false });