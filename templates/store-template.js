import { persistReducer } from "redux-persist";
import storage from 'redux-persist/lib/storage';
import { put, takeEvery } from "redux-saga/effects";

export const actionTypes = {
    get<moduleName>: `GET_<moduleName>`,
    set<moduleName>: "SET_<moduleName>",
};

const initialState = {
    <moduleName>: [],
}

const <moduleName>Reducer = ( state = initialState, action ) => {
      switch ( action.type ) {
        case actionTypes.set<moduleName>:
        return {
            ...state, <moduleName>: action.payload?.<moduleName>
        } 
        default:
            return state;
    }
}

export const actions = {
    get<moduleName>: () => ({
        type: actionTypes.get<moduleName>,
    }),
    set<moduleName>s: ( <moduleName> ) => ({
        type: actionTypes.set<moduleName>,
        payload: { <moduleName> }
    })   
}

export function* <moduleName>Saga () {
    yield takeEvery(actionTypes.get<moduleName>, function* saga () {
    });
}

const persistConfig = {
   
}

export default persistReducer( persistConfig, <moduleName>Reducer );